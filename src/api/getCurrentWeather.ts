import { request } from './request';

type Params = {
  lat: number;
  lon: number;
  excludes?: string[];
};

export const getCurrentWeather = ({ lat, lon, excludes }: Params) =>
  request(
    `/onecall?lat=${lat}&lon=${lon}&exclude=${excludes?.join(',') || ''}`,
    {
      method: 'GET',
    }
  );
