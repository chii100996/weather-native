import { StyleSheet } from "react-native";
import { colors } from "../../constants";

const styles = StyleSheet.create({
  placeholder: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
  image: {
    width: 200,
    height: 200,
  },
  text: {
    fontSize: 16,
    lineHeight: 24,
    color: colors.secondary,
    fontFamily: "Ubuntu-Bold",
    textAlign: "center",
    marginTop: 24,
  },
});

export default styles;
