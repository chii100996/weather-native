import React from "react";
import styles from "./SectionPlaceholder.styles";
import { Text, View, Image } from "react-native";

export const SectionPlaceholder = () => {
  return (
    <View style={styles.placeholder}>
      <Image
        style={styles.image}
        source={require("../../../assets/images/placeholder.png")}
      />
      <Text style={styles.text}>
        Fill in all the fields and the weather will be displayed
      </Text>
    </View>
  );
};
