import { StyleSheet } from "react-native";
import { colors } from "../../constants";

const styles = StyleSheet.create({
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 16,
  },
  footerName: {
    fontSize: 14,
    lineHeight: 18,
    color: colors.baseWeak,
    opacity: 0.6,
  },
});

export default styles;
