import React from 'react';
import styles from './Footer.styles';
import { Text, View } from "react-native";

export const Footer = () => (
  <View style={styles.footer}>
    <Text style={styles.footerName}>
      C ЛЮБОВЬЮ ДЛЯ MERCURY DEVELOPMENT
    </Text>
  </View>
);
