import React from "react";
import { View } from "react-native";
import { Picker } from '@react-native-picker/picker';
import { TPoint } from "../../types";
import styles from "./SelectCity.styles";

type Props = {
  city: TPoint | null;
  cities: TPoint[];
  onChangeCity: (id: number) => void;
};

export const SelectCity: React.FC<Props> = ({ cities, city, onChangeCity }) => {
  const onSelect = (name: string) => {
    const city = cities.find((c) => c.name === name);
    city && onChangeCity(city.id);
  };
  return (
    <View style={styles.selector}>
      <View>
        <Picker
          style={styles.content}
          onValueChange={(itemValue: string) => onSelect(itemValue)}
        >
          <Picker.Item label={'Select city'} enabled={false} />
          {cities.map((c) => (
            <Picker.Item key={c.id} label={c.name || ""} value={c.name ?? ''} />
          ))}
        </Picker>
      </View>
    </View>
  );
};
