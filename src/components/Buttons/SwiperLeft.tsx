import React from 'react';
import chevronLeft from 'assets/icons/chevron-left.svg';
import style from './Buttons.module.scss';

type Props = {
  onClick: () => void;
  disable?: boolean;
};

export const SwiperLeft: React.FC<Props> = ({ onClick, disable }) => (
  <button
    type="button"
    onClick={onClick}
    className={`${style.button} ${disable ? style.button_disable : ''}`}
  >
    <img src={chevronLeft} alt="chevronLeft" className={style.button__icon} />
  </button>
);
