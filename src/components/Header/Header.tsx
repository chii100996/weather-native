import React from "react";
import styles from "./Header.styles";
import { Text, View } from "react-native";

export const Header = () => (
  <View style={styles.header}>
    <View style={styles.headerTitle}>
      <Text style={styles.titleItemTop}>Weather</Text>
      <View>
        <Text style={styles.titleItemBottom}>forecast</Text>
      </View>
    </View>
  </View>
);
