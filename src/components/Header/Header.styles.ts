import { StyleSheet } from "react-native";
import { colors } from "../../constants";

const styles = StyleSheet.create({
  header: {
    justifyContent: "center",
    paddingTop: 32,
    marginBottom: 54,
  },
  headerTitle: {
    paddingTop: 32,
    alignContent: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  titleItemBottom: {
    color: colors.baseWeak,
    fontSize: 32,
    lineHeight: 30,
    fontFamily: "Ubuntu-Bold",
    position: "relative",
    top: 30,
    left: -16,
    zIndex: 5,
  },
  titleItemTop: {
    color: colors.baseWeak,
    fontSize: 32,
    lineHeight: 30,
    fontFamily: "Ubuntu-Bold",
  },
});

export default styles;
