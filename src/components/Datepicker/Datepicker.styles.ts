import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  selector: {
    height: 48,
    borderColor: "rgba(128, 131, 164, 0.2)",
    borderWidth: 2,
    borderRadius: 8,
    justifyContent: "center",
    paddingLeft: 16,
  },
  wrapper: {
    flex: 1,
    marginTop: 24,
  },
  text: {
    fontFamily: "Ubuntu-Regular",
  },
});

export default styles;
