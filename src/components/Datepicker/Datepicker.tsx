import moment from "moment";
import React, { useState } from "react";
import DateTimePicker from "@react-native-community/datetimepicker";
import { View, Text, TouchableOpacity, Platform } from "react-native";
import styles from "./Datepicker.styles";

type Props = {
  date: number | null;
  onSelect: (date: number) => void;
};

export const Datepicker: React.FC<Props> = ({ date, onSelect }) => {
  const [show, setShow] = useState(false);
  const minDate = new Date(moment().add(-4, "days").format("YYYY-MM-DD"));
  const maxDate = new Date(moment().format("YYYY-MM-DD"));
  return (
    <View style={styles.wrapper}>
      {show && (
        <DateTimePicker
          value={
            new Date(
              date
                ? moment(date).format("YYYY-MM-DD")
                : moment().format("YYYY-MM-DD")
            )
          }
          is24Hour={true}
          display="default"
          onChange={(props: any) => {
            onSelect(moment(props?.nativeEvent?.timestamp).unix() * 1000);
            setShow(Platform.OS === "ios");
          }}
          minimumDate={minDate}
          maximumDate={maxDate}
        />
      )}
      <TouchableOpacity onPress={() => setShow(true)}>
        <View style={styles.selector}>
          <Text style={styles.text}>
            {date ? moment(date).format("DD/MM/YYYY") : "Select date"}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
