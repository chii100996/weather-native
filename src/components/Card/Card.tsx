import React from "react";
import moment from "moment";
import { getIconSrc } from "../../api";
import styles from "./Card.styles";
import { View, Text, Image } from "react-native";

type Props = {
  date: number;
  image: string;
  temperature: number;
  bigIcon?: boolean;
};

export const Card: React.FC<Props> = ({
  date,
  image,
  temperature,
  bigIcon,
}) => (
  <View style={bigIcon ? styles.card : [styles.card, styles.bigCard]}>
    <Text style={styles.date}>{`${moment(date * 1000)
      .format("DD MMM YYYY")
      .toLowerCase()}`}</Text>
    <View style={styles.imageWrapper}>
      <Image style={styles.image} source={{ uri: getIconSrc(image) }} />
    </View>
    <Text style={styles.temperature}>
      {`${temperature >= 0 ? "+" : "-"}${Math.floor(temperature)}`}&deg;
    </Text>
  </View>
);
