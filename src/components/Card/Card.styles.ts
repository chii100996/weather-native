import { StyleSheet } from "react-native";
import { colors } from "../../constants";

const styles = StyleSheet.create({
  card: {
    backgroundColor: colors.accent,
    height: 238,
    fontFamily: "Ubuntu-Bold",
    borderWidth: 2,
    borderColor: colors.baseStrong,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    flex: 1,
    marginHorizontal: 5,
  },
  bigCard: {
    flex: 1,
  },
  date: {
    fontFamily: "Ubuntu-Bold",
    fontSize: 16,
    lineHeight: 24,
    alignSelf: "flex-start",
    margin: 0,
    color: colors.baseWeak,
  },
  imageWrapper: {
    justifyContent: "center",
    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
  },
  imageBig: {
    width: 160,
  },
  temperature: {
    fontFamily: "Ubuntu-Bold",
    alignSelf: "flex-end",
    fontSize: 32,
    lineHeight: 32,
    margin: 0,
    color: colors.baseWeak,
  },
});

export default styles;
