import { StyleSheet } from "react-native";
import { colors } from "../../constants";

const styles = StyleSheet.create({
  section: {
    backgroundColor: colors.baseWeak,
    borderRadius: 8,
    paddingVertical: 32,
    paddingHorizontal: 24,
    marginTop: 0,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  sectionTitle: {
    color: colors.baseStrong,
    fontSize: 32,
    fontFamily: "Ubuntu-Bold",
    marginRight: 32,
    lineHeight: 32,
  },
  sectionMain: {
    alignItems: "center",
    overflow: "scroll",
    justifyContent: "flex-start",
  },
  sectionControls: {
    justifyContent: "center",
    flexWrap: "wrap",
    marginBottom: 25,
  },
});

export default styles;
