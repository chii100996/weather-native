import React from 'react';
import { View, Text } from 'react-native';
import styles from './Section.styles';

type Props = {
  title: string;
  controls: JSX.Element;
};

export const Section: React.FC<Props> = ({ title, controls, children }) => {
  const onSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
  };
  return (
    <View style={styles.section}>
      <Text style={styles.sectionTitle}>{title}</Text>
      <View style={styles.sectionControls} 
      // onSubmit={onSubmit}
      >
        {controls}
      </View>
      <View style={styles.sectionMain}>{children}</View>
    </View>
  );
};
