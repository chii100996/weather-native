import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  main: {
    flexGrow: 1,
    width: "100%",
  },
  mainContent: {
    flexDirection: "row",
  },
  control: {
    flexDirection: "row",
  },
});

export default styles;
