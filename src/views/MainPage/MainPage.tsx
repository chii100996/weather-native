import React from "react";
import { Datepicker } from "../../components/Datepicker";
import { Section } from "../../components/Section";
import { SelectCity } from "../../components/SelectCity";
import { observer } from "mobx-react";
import store from "../../store";
import { SectionPlaceholder } from "../../components/SectionPlaceholder";
import { Card } from "../../components/Card";
import { View, ScrollView } from "react-native";
import styles from "./Main.styles";

const MainPage: React.FC = () => (
  <View style={styles.main}>
    <Section
      title="7 Days Forecast"
      controls={
        <View style={styles.control}>
          <SelectCity
            city={store.selectedPoint}
            cities={store.points}
            onChangeCity={(id) => {
              store.selectPoint(id);
              store.fetchDaily();
            }}
          />
        </View>
      }
    >
      {store.daily.length ? (
        <ScrollView horizontal>
          {store.daily.map((d) => (
            <Card
              key={d.dt}
              date={d.dt}
              image={d.weather[0]?.icon}
              temperature={d.temp.day}
            />
          ))}
        </ScrollView>
      ) : (
        <SectionPlaceholder />
      )}
    </Section>
    <Section
      title="Forecast for a Date in the Past"
      controls={
        <View style={styles.main}>
          <View style={styles.control}>
            <SelectCity
              city={store.selectedHistoryPoint}
              cities={store.points}
              onChangeCity={(id) => {
                store.selectHistoryPoint(id);
                store.fetchHistory();
              }}
            />
          </View>
          <View style={styles.control}>
            <Datepicker
              date={store.selectedHistoryDate}
              onSelect={(date) => {
                store.selectHistoryDate(date);
                store.fetchHistory();
              }}
            />
          </View>
        </View>
      }
    >
      <View style={styles.mainContent}>
        {store.history && store.history.dt && store.history.temp ? (
          <Card
            key={store.history.dt}
            date={store.history.dt}
            image={store.history.weather[0]?.icon}
            temperature={store.history.temp}
            bigIcon
          />
        ) : (
          <SectionPlaceholder />
        )}
      </View>
    </Section>
  </View>
);

export default observer(MainPage);
