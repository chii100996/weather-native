import { types } from 'mobx-state-tree';

export const MTemp = types.model({
  day: types.number,
  min: types.number,
  max: types.number,
  night: types.number,
  eve: types.number,
  morn: types.number,
});
