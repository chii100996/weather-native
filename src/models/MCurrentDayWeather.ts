import { types } from 'mobx-state-tree';
import { MWeather } from './MWeather';

export const MCurrentDayWeather = types.model({
  dt: types.maybeNull(types.number),
  sunrise: types.maybeNull(types.number),
  sunset: types.maybeNull(types.number),
  temp: types.maybeNull(types.number),
  feels_like: types.maybeNull(types.number),
  pressure: types.maybeNull(types.number),
  humidity: types.maybeNull(types.number),
  dew_point: types.maybeNull(types.number),
  uvi: types.maybeNull(types.number),
  clouds: types.maybeNull(types.number),
  visibility: types.maybeNull(types.number),
  wind_speed: types.maybeNull(types.number),
  wind_deg: types.maybeNull(types.number),
  weather: types.array(MWeather),
});
