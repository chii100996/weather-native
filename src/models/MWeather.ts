import { types } from 'mobx-state-tree';

export const MWeather = types.model({
  id: types.number,
  main: types.string,
  description: types.string,
  icon: types.string,
});
