import { types } from 'mobx-state-tree';
import { MFeelsLike } from './MFeelsLike';
import { MTemp } from './MTemp';
import { MWeather } from './MWeather';

export const MDailyWeather = types.model({
  dt: types.number,
  sunrise: types.number,
  sunset: types.number,
  moonrise: types.number,
  moonset: types.number,
  moon_phase: types.number,
  temp: MTemp,
  feels_like: MFeelsLike,
  pressure: types.number,
  humidity: types.number,
  dew_point: types.number,
  wind_speed: types.number,
  wind_deg: types.number,
  weather: types.array(MWeather),
  clouds: types.number,
  pop: types.number,
  uvi: types.number,
});
