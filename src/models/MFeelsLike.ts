import { types } from 'mobx-state-tree';

export const MFeelsLike = types.model({
  day: types.number,
  night: types.number,
  eve: types.number,
  morn: types.number,
});
