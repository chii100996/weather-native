import { Instance } from 'mobx-state-tree';
import { MPoint } from '../models';

export type TPoint = Instance<typeof MPoint>;
