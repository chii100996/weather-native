import { points } from '../constants/points';
import Store from './Store';

const store = Store.create({ points });

export default store;
