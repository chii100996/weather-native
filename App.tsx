import React from "react";
import { View, ScrollView, ImageBackground } from "react-native";
import { Footer } from "./src/components/Footer";
import MainPage from "./src/views/MainPage";
import { Header } from "./src/components/Header";
import styles from "./App.styles";
import { useFonts } from "expo-font";

function App() {
  const [loaded] = useFonts({
    "Ubuntu-Bold": require("./assets/fonts/Ubuntu-Bold.ttf"),
    "Ubuntu-Regular": require("./assets/fonts/Ubuntu-Regular.ttf"),
  });

  if (!loaded) {
    return null;
  }
  return (
    <View style={styles.app}>
      <ImageBackground
        source={require("./assets/images/bg-top.png")}
        style={styles.background}
      >
        <ImageBackground
          source={require("./assets/images/bg-bottom.png")}
          style={styles.background}
        >
          <ScrollView>
            <Header />
            <MainPage />
            <Footer />
          </ScrollView>
        </ImageBackground>
      </ImageBackground>
    </View>
  );
}

export default App;
