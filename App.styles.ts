import { colors } from "./src/constants";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  app: {
    position: "relative",
    minHeight: "100%",
    backgroundColor: colors.accent,
    color: colors.baseWeak,
  },
  background: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
});

export default styles;
